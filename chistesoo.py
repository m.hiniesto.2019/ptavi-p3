#!/usr/bin/python3
# -*- coding: utf-8 -*-


import xml.dom.minidom


class Humor():


    def __init__(self, path):
        self.path = path


    def jokes(self):
        """Programa principal"""
        document = xml.dom.minidom.parse(self.path)
        jokes = document.getElementsByTagName('chiste')
        chiste = {}
        calificaciones = ["buenisisimo", "bueno", "regular", "malo", "malisimo"]

        for joke in jokes:
            score = joke.getAttribute('calificacion')
            questions = joke.getElementsByTagName('pregunta')
            question = questions[0].firstChild.nodeValue.strip()
            answers = joke.getElementsByTagName('respuesta')
            answer = answers[0].firstChild.nodeValue.strip()
            chiste[score] = [question, answer]

        for califi in calificaciones:
            calificacion = list(filter(lambda x: x == califi, chiste))

            try:
                cal = calificacion[0]
            except:
                pass
            else:
                califi == cal
                print(f"Calificacion: {cal}")
                print(f"Pregunta: {chiste.get(cal)[0]}")
                print(f"Respuesta: {chiste.get(cal)[1]} \n")

if __name__ == "__main__":
        Broma = Humor("chistes.xml")
        Broma.jokes()